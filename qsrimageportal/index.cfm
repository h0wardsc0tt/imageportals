<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>QSR Image Library - HME</title>
	<link type="text/css" href="styles.css" rel="stylesheet" />
</head>

<body>
	<table cellpadding="0" cellspacing="0" class="productTable" width="492">
		<tr>
			<td colspan="3"><a name="top"></a></td>
		</tr>
		<tr>
			<td colspan="3">
            <img src="images/qsrheader.jpg" border="0" alt="PRO AUDIO GRAPHIC RESOURCES" width="490" height="52" /><br>
            The purpose of this portal is to assist dealers and distributors to promote the sale of HME  products. Prior to publicly using any content from this portal, you must obtain written permission from HME, Inc. All rights reserved. No part of this website may be reproduced or transmitted in any form or by any means without prior permission in writing from HME, Inc.<br>
            <br>
            <a href="HME_Legal_Guidelines_QSR.pdf"><img src="images/HMETrademark.jpg" width="304" height="15" border="0"></a><br>
            <br>
All rights reserved. No part of this website may be reproduced or transmitted in any form or by any means without prior permission in writing from HME, Inc.
<p>&nbsp;</p></td></tr>	
		<tr>
			<td colspan="3">
				<table width="492" cellpadding="0" cellspacing="0">
					<tr>
						<td width="50%">
							<a href="#TEMPLATES" class="listLink">MARKETING COLLATERAL TEMPLATES</a><a href="#ION" class="listLink">ION IQ IMAGES </a><a href="#wirelessIQ" class="listLink">WIRELESS IQ IMAGES</a><a href="#odysseyIQ" class="listLink">ODYSSEY IQ IMAGES</a>
							<a href="#system30a" class="listLink">SYSTEM 30A TIMER IMAGES</a>
							<a href="#hmecorporatefiles" class="listLink">HME CORPORATE FILES</a><a href="#accessories" class="listLink"></a>						</td>
						<td width="50%"><p>&nbsp;</p>
							<p>&nbsp;</p>
					  <p>&nbsp;</p></td>
					</tr>
				</table>			</td>
		</tr>		
		<tr>
		  <td colspan="3"><a name="TEMPLATES" id="ION2"> <img src="COLLATERAL_TEMPLATES/Header.jpg" border="0" alt="PRO850 IMAGES" class="productHeadingImg" width="491" height="24"/></a></td>
	  </tr>
		<tr valign="top">
		  <td><div class="linksDiv"> <img src="COLLATERAL_TEMPLATES/Thumbnails/ION-IQ-Brochure-Thumb.jpg" alt="" width="139" height="100" />
		    <h3>ION IQ BROCHURE </h3>
		    <a href="COLLATERAL_TEMPLATES/Word Docs/ION-IQ-Brochure-Word-Template.doc" class="listLink">Word Template</a></div></td>
		  <td><div class="linksDiv"><img src="COLLATERAL_TEMPLATES/Thumbnails/ZOOM-Brochure-Thumb.jpg" alt="" width="139" height="100" align="absmiddle" /><br>
		    <h3> ZOOM BROCHURE</h3>
		    <a href="COLLATERAL_TEMPLATES/Word Docs/ZOOM-Brochure-Word-Template.doc" class="listLink">Word Template</a></div></td>
		  <td><div class="linksDiv"><img src="COLLATERAL_TEMPLATES/Thumbnails/DASH-Brochure-Thumb.jpg" alt="" width="139" height="100" align="absmiddle" /><br>
		    <h3> DASH BROCHURE</h3>
		    <a href="COLLATERAL_TEMPLATES/Word Docs/DASH-Brochure-Word-Template.doc" class="listLink">Word Template</a></div></td>
	  </tr>
		<tr>
		  <td colspan="3">&nbsp;</td>
	  </tr>
		<tr>
		  <td colspan="3">&nbsp;</td>
	  </tr>
		<tr>
			<td colspan="3"><a name="ION" id="ION">
            <img src="images/Headers/ioniqheader.jpg" border="0" alt="PRO850 IMAGES" class="productHeadingImg" width="491" height="24"/></a></td>	
		</tr>			
		
		
		
		<tr valign="top">
			<td>
				<div class="linksDiv">
				  <img src="images/Thumbs/ION-IQ-Base-Station.jpg" alt="" width="100" height="100" />
				  <h3>ION IQ Base Station (image only)</h3>					
					<a href="images/Low-REZ/ION-IQ-Base-Station.jpg" class="listLink">Low Resolution JPG</a>
					<a href="images/High-REZ/ION-IQ-Base-Station.tif" class="listLink">High Resolution TIF</a>				</div>		  </td>	
			<td>
				<div class="linksDiv"><img src="images/Thumbs/ION-AIO-Headset-01.jpg" alt="" width="100" height="100" align="absmiddle" /><br>
		          <h3> All-In-One Headset 1 (image only)</h3>
					<a href="images/Low-REZ/ION-AIO-Headset-01.jpg" class="listLink">Low Resolution JPG</a>
					<a href="images/High-REZ/ION-AIO-Headset-01.tif" class="listLink">High Resolution TIF</a>				</div>		  </td>	
			<td><div class="linksDiv"><img src="images/Thumbs/ION-AIO-Headset-02.jpg" alt="" width="100" height="100" align="absmiddle" /><br>
			  <h3> All-In-One Headset 2 (image only)</h3>
		    <a href="images/Low-REZ/ION-AIO-Headset-02.jpg" class="listLink">Low Resolution JPG</a> <a href="images/High-REZ/ION-AIO-Headset-02.tif" class="listLink">High Resolution TIF</a></div></td>	
		</tr>
		<tr valign="top">
          <td><div class="linksDiv"> <img src="images/Thumbs/ION-IQ-Belt-Pac.jpg" alt="" width="100" height="100" />
            <h3>Belt-Pac (image only)<br>
            </h3>
          <a href="images/Low-REZ/ION-IQ-Belt-Pac.jpg" class="listLink">Low Resolution JPG</a> <a href="images/High-REZ/ION-IQ-Belt-Pac.tif" class="listLink">High Resolution TIF</a></div></td>
		  <td><div class="linksDiv"> <img src="images/Thumbs/ION-IQ+base-vertical.jpg" alt="image" width="100" height="100" />
		    <h3>ION IQ Base Station/Tag Line (vertical format)</h3>
	      <a href="images/Low-REZ/ION-IQ+base-vertical.jpg" class="listLink">Low Resolution JPG</a> <a href="images/High-REZ/ION-IQ+base-vertical.tif" class="listLink">High Resolution TIF</a></div></td>
		  <td><div class="linksDiv"> <img src="images/Thumbs/ION-IQ+base-horizontal.jpg" alt="Image" width="100" height="100" />
		    <h3>ION IQ Base Station/Tag Line (horizontal format)</h3>
	      <a href="images/Low-REZ/ION-IQ+base-horizontal.jpg" class="listLink">Low Resolution JPG</a> <a href="images/High-REZ/ION-IQ+base-horizontal.tif" class="listLink">High Resolution TIF</a></div></td>
		</tr>
		<tr valign="top">
          <td><div class="linksDiv"> <img src="images/Thumbs/ION-IQ-horizontal.jpg" alt="image" width="100" height="100" />
            <h3>ION IQ Tag Line (horizontal format)<br>
            </h3>
          <a href="images/Low-REZ/ION-IQ-horizontal.jpg" class="listLink">Low Resolution JPG</a> <a href="images/High-REZ/ION-IQ-horizontal.tif" class="listLink">High Resolution TIF</a></div></td>
          <td><div class="linksDiv"> <img src="images/Thumbs/ION-IQ-vertical.jpg" alt="Image" width="100" height="100" />
            <h3>ION IQ Tag Line (vertical format)<br>
            </h3>
          <a href="images/Low-REZ/ION-IQ-vertical.jpg" class="listLink">Low Resolution JPG</a> <a href="images/High-REZ/ION-IQ-vertical.tif" class="listLink">High Resolution TIF</a></div></td>
		  <td>&nbsp;</td>
	  </tr>
		<tr>
          <td colspan="3"><a name="wirelessIQ" id="wirelessIQ"> <br>
            <br>
            <br>
            <br>
            <br>
            <br>
          <img src="images/wirelessiqheader.jpg" border="0" alt="PRO850 IMAGES" class="productHeadingImg" width="491" height="24"/></a></td>
	  </tr>
		<tr valign="top">
          <td><div class="linksDiv"> <img src="Wireless IQ Images-1/Headset_Adapter_Product_Shot_tn.jpg" alt="Image" width="100" height="86" />
                  <h3>Headset Adapter</h3>
            <a href="Wireless IQ Images-1/Headset Adapter Product Shot 1.jpg" class="listLink">Low Resolution JPG</a> <a href="Wireless IQ Images-1/Headset Adapter Product Shot 1.tif" class="listLink">High Resolution TIF</a> </div></td>
		  <td><div class="linksDiv"><img src="Wireless IQ Images-1/Telephone_Interface_Product_Shot_tn.jpg" alt="Image" width="101" height="69" align="absmiddle" /><br>
                  <h3>&nbsp;</h3>
		    <h3>Telephone Interface</h3>
		    <a href="Wireless IQ Images-1/Telephone Interface Product Shot 1.jpg" class="listLink">Low Resolution JPG</a> <a href="Wireless IQ Images-1/Telephone Interface Product Shot 1.tif" class="listLink">High Resolution TIF</a> </div></td>
		  <td><div class="linksDiv"> <img src="Wireless IQ Images-1/Wireless_IQ_Product_Shot_tn.jpg" alt="Image" width="100" height="86" />
                  <h3>Wireless IQ</h3>
		    <a href="Wireless IQ Images-1/Wireless IQ Product Shot 1.jpg" class="listLink">Low Resolution JPG</a> <a href="Wireless IQ Images-1/Wireless IQ Product Shot 1.tif" class="listLink">High Resolution TIF</a> </div></td>
	  </tr>
		<tr valign="top">
          <td><div class="linksDiv"> <img src="Wireless IQ Images-1/WIQBS-thumb.jpg" alt="Image" width="100" height="100" />
                  <h3>WIQ Base Station</h3>
            <a href="Wireless IQ Images-1/WIQBS.jpg" class="listLink">Low Resolution JPG</a> <a href="Wireless IQ Images-1/WIQBS.tif" class="listLink">High Resolution TIF</a> </div></td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
		<tr>
          <td colspan="3">&nbsp;
              <p><a href="#top" class="listLink">Back To Top</a></p></td>
	  </tr>
		<tr>
		  <td colspan="3">&nbsp;<p><a href="#top" class="listLink">Back To Top</a></p>				
	      <br></td>
			</tr>
		</tr>
		
		
		
		
		<tr>
			<td colspan="3"><a name="odysseyIQ" id="odysseyIQ">
            <img src="images/odiqheader.jpg" border="0" alt="DX200 IMAGES" class="productHeadingImg" width="491" height="24"/></a></td>	
		</tr>
		<tr valign="top">
			<td>
				<div class="linksDiv">
					<img src="Odyssey IQ Images-2/Odyssey IQ Listen Only Headset Product Shot_tn.jpg" alt="" width="65" height="103" />
					<h3>Odyssey IQ Listen Only Headset</h3>					
					<a href="Odyssey IQ Images-2/Odyssey IQ Listen Only Headset Product Shot 1.jpg" class="listLink">Low Resolution JPG</a>
					<a href="Odyssey IQ Images-2/Odyssey IQ Listen Only Headset Product Shot 1.tif" class="listLink">High Resolution TIF</a>				</div>			</td>	
			<td>
				<div class="linksDiv">
					<img src="Odyssey IQ Images-2/Odyssey IQ Product Shot 1_tn.jpg" alt="" width="100" height="103" />
					<h3>Odyssey IQ Product<br>
					  (Right VIEW)<br>
					</h3>
					<a href="Odyssey IQ Images-2/Odyssey IQ Product Shot 1.jpg" class="listLink">Low Resolution JPG</a>
					<a href="Odyssey IQ Images-2/Odyssey IQ Product Shot 1.tif" class="listLink">High Resolution TIF</a>				</div>			</td>	
			<td>
				<div class="linksDiv">
					<img src="Odyssey IQ Images-2/Odyssey IQ Product Shot 2_tn.jpg" alt="" width="100" height="102" />
					<h3>Odyssey IQ Product<br>
					  (LEFT VIEW)<br>
					</h3>
					<a href="Odyssey IQ Images-2/Odyssey IQ Product Shot 2.jpg" class="listLink">Low Resolution JPG</a>
					<a href="Odyssey IQ Images-2/Odyssey IQ Product Shot 2.tif" class="listLink">High Resolution TIF</a>				</div>			</td>	
		</tr>
		<tr valign="top">	
			<td>
				<div class="linksDiv">
					<img src="Odyssey IQ Images-2/Odyssey IQ With Battery_tn.jpg" alt="" width="100" height="103" />
					<h3>Odyssey IQ With Battery</h3>					
					<a href="Odyssey IQ Images-2/Odyssey IQ With Battery.jpg" class="listLink">Low Resolution JPG</a>
					<a href="Odyssey IQ Images-2/Odyssey IQ With Battery.tif" class="listLink">High Resolution TIF</a>				</div>			</td>	
			<td><div class="linksDiv"> <img src="Wireless IQ Images-1/WIQBS-thumb.jpg" alt="" width="100" height="103" />
                  <h3>WIQ Base Station<br>
              </h3>
		    <a href="Wireless IQ Images-1/WIQBS.jpg" class="listLink">Low Resolution JPG</a> <a href="Wireless IQ Images-1/WIQBS.tif" class="listLink">High Resolution TIF</a> </div></td>	
			<td>&nbsp;</td>	
		</tr>
		<tr>
			<td colspan="3">&nbsp;<p><a href="#top" class="listLink">Back To Top</a></p>		    </td>
		</tr>
		
		
<!-- begin product header template -->		
		<tr>
			<td colspan="3">
              <p><a name="system30a" id="system30a"></a></p>
            <p><img src="images/system30header.jpg" border="0" alt="DX100 IMAGES" class="productHeadingImg" width="491" height="24"/></p>            </td>	
		</tr>		
<!-- end product header template -->	
		
<!-- begin table row template -->
		<tr valign="top">
			<td>
				<div class="linksDiv">
					<img src="System 30A Timer-3/System 30A Timer with Tape_tn.jpg" alt="" width="100" height="131" />
					<h3>System 30A Timer with Tape</h3>					
					<a href="System 30A Timer-3/System 30A Timer with Tape.jpg" class="listLink">Low Resolution JPG</a>
					<a href="System 30A Timer-3/System 30A Timer with Tape.tif" class="listLink">High Resolution TIF</a>				</div>			</td>	
			<td>
				<div class="linksDiv">
					<img src="System 30A Timer-3/System 30A Timer_tn.jpg" alt="" width="100" height="86" />
					<br>
					<br>
					<br>
					<br>
					<h3>System 30A TimeR<br>
					      <br>
			          </h3>
					<a href="System 30A Timer-3/System 30A Timer.jpg" class="listLink">Low Resolution JPG</a>
					<a href="System 30A Timer-3/System 30A Timer.tif" class="listLink">High Resolution TIF</a>				</div>			</td>	
			<td>
				<div class="linksDiv"></div>			</td>
		</tr>
		<tr valign="top">
          <td><div class="linksDiv"> <img src="System 30A Timer-3/Display_GREENtn.jpg" alt="1" width="100" height="65" /> <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <h3>DISPLAY GREEN<br>
                  </h3>
            <a href="System 30A Timer-3/Display_GREEN.jpg" class="listLink">Low Resolution JPG</a> <a href="System 30A Timer-3/Display_GREEN.tif" class="listLink">High Resolution TIF</a> </div></td>
		  <td><div class="linksDiv"> <img src="System 30A Timer-3/Display_REDtn.jpg" alt="2" width="100" height="65" /> <br>
                  <br>
                  <br>
                  <br>
                  <h3><br>
                      <br>
                    DISPLAY RED <br>
                  </h3>
		    <a href="System 30A Timer-3/Display_RED.jpg" class="listLink">Low Resolution JPG</a> <a href="System 30A Timer-3/Display_RED.tif" class="listLink">High Resolution TIF</a> </div></td>
		  <td><div class="linksDiv"></div></td>
	  </tr>
		<tr>
			<td height="60" colspan="3">&nbsp;
		    <p><a href="#top" class="listLink">Back To Top</a></p></td>
		</tr>

			
<!-- begin product header template -->		
<!-- end product header template -->
			
<!-- begin product header template -->		
<!-- end product header template -->



<!--Begin product header template-->		
<!-- end product header template -->

<!--start table row template-->

<!--end table row template-->
<!--Begin product header template-->	
<!--end product header template-->

<!--begin table row template-->
		
        <tr>
			<td colspan="3"><a name="hmecorporatefiles">
            <img src="images/HMEcorpFilesHeader.jpg" border="0" alt="HME CORPORATE FILES" class="productHeadingImg" width="490" height="23"/></a></td>	
		</tr>		
		<tr valign="top">
			<td>
				<div class="linksDiv">
					<img src="images/th-HME.jpg" alt="" width="100" height="100" />
					<h3>HME Logo B&amp;W</h3>		
					<a href="HME_Corporate_Files/B_W_Logos/No_Tag_B_W/HME_Logo_B_W.eps" class="listLink">Low Resolution EPS</a>			
					<a href="HME_Corporate_Files/B_W_Logos/No_Tag_B_W/HME_Logo_B_W.jpg" class="listLink">Low Resolution JPG</a>
					<a href="HME_Corporate_Files/B_W_Logos/No_Tag_B_W/HME_Logo_B_W.tif" class="listLink">High Resolution TIF</a>				</div>			</td>	
			<td>
				<div class="linksDiv">
					<img src="images/th-HMEbwTag.jpg" alt="" width="100" height="100" />
					<h3>HME Logo B&amp;W Tag</h3>	
					<a href="HME_Corporate_Files/B_W_Logos/Tag_B_W/HME_Logo_B_W_Tag.eps" class="listLink">Low Resolution EPS</a>				
					<a href="HME_Corporate_Files/B_W_Logos/Tag_B_W/HME_Logo_B_W_Tag.jpg" class="listLink">Low Resolution JPG</a>
					<a href="HME_Corporate_Files/B_W_Logos/Tag_B_W/HME_Logo_B_W_Tag.tif" class="listLink">High Resolution TIF</a>				</div>			</td>	
			<td>
				<div class="linksDiv">
					<img src="images/th-hmelogo281.jpg" alt="" width="100" height="100" />
					<h3>HME Logo 281</h3>
					<a href="HME_Corporate_Files/Color_Logos/No_Tag_Color/HME_Logo_281.eps" class="listLink">High Resolution EPS</a>					
					<a href="HME_Corporate_Files/Color_Logos/No_Tag_Color/HME_Logo_281.jpg" class="listLink">Low Resolution JPG</a>
					<a href="HME_Corporate_Files/Color_Logos/No_Tag_Color/HME_Logo_281.tif" class="listLink">High Resolution TIF</a>
					<a href="HME_Corporate_Files/Color_Logos/No_Tag_Color/HME_Logo_281.png" class="listLink">Low Resolution PNG</a>				</div>			</td>	
		</tr>
		<tr valign="top">
			<td>
				<div class="linksDiv">
					<img src="images/th-hmelogo281tag.jpg" alt="" width="100" height="100" />
					<h3>HME Logo 281 Tag</h3>					
					<a href="HME_Corporate_Files/Color_Logos/Tag_Color/HME_Logo_281_Tag.eps" class="listLink">High Resolution EPS</a>
					<a href="HME_Corporate_Files/Color_Logos/Tag_Color/HME_Logo_281_Tag.jpg" class="listLink">Low Resolution JPG</a>
					<a href="HME_Corporate_Files/Color_Logos/Tag_Color/HME_Logo_281_Tag.tif" class="listLink">High Resolution TIF</a>				</div>			</td>	
			<td colspan="2">&nbsp;			</td>	
		</tr>
		<tr>
			<td colspan="3">&nbsp;<p><a href="#top" class="listLink">Back To Top</a></p> </td>
		</tr>

		
<!--end table row template-->						
	</table>


</body>
</html>