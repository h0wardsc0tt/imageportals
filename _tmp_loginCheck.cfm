<cfsilent>
<!---2012/06/08 ATN: Security Check --->
<cfscript>
	if(FORM.j_username IS NOT "") {
		if(REFindNoCase('[\(\)\*\|;\"@=\?<>:\/\\,]', FORM.j_username) GT 0) { //search for bad chard
			ERR.ErrorFound = true;
			ERR.ErrorMessage = "Invalid credentials supplied. Please try again";
		}
	}
	if(FORM.j_password IS NOT "") {
		if(Len(FORM.j_password) LT 4 OR ReFindNoCase("(cn=|[ ])", FORM.j_password) GT 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = "Invalid credentials supplied. Please try again";
		}
	}
</cfscript>
<cfif NOT ERR.ErrorFound>
	<cftry>
		<cfscript>
			VUSER = "";
			VPASS = "";
			if(CGI.CF_TEMPLATE_PATH CONTAINS "proaudioimages") {
				VUSER = "HMEPRO~HMEQSR";
				vPASS = "2007GR!~2012!HMEQSR";
			}
			if(CGI.CF_TEMPLATE_PATH CONTAINS "pcdimages") {
				VUSER = "HMEPCD";
				VPASS = "2007GR";
			}
			if(CGI.CF_TEMPLATE_PATH CONTAINS "qsrimages") {
				VUSER = "HMEQSR";
				VPASS = "2007GR!";
			}
			if(CGI.CF_TEMPLATE_PATH CONTAINS "qsrimageportal") {
				VUSER = "HMEQSR";
				VPASS = "2007GR!";
			}
			if(Len(VUSER) EQ 0 OR Len(VPASS) EQ 0) { //NO VALID TEMPLATES FOUND
				ERR.ErrorFound = true;
				ERR.ErrorMessage = "Login fail. User could not be authenticated.";
			} else {
				if(ListFind(VUSER, FORM.j_username, "~") EQ 0 OR ListFind(VPASS, FORM.j_password, "~") EQ 0) { //LOGIN FAILS
					ERR.ErrorFound = true;
					ERR.ErrorMessage = "Login fail. User could not be authenticated.";
				}
			}
		</cfscript>
		
		<cfcatch type="any">
			<cfset ERR.ErrorFound = true>
			<cfset ERR.ErrorMessage = "Login fail. The username or password you entered was not found in our system.<br/>If this problem persists, please contact the system administrator.">
		</cfcatch>
	</cftry>
</cfif>
</cfsilent>